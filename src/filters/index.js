import Vue from 'vue'
import Dayjs from 'dayjs'
const dateFormat = (value) =>{
    if (!value) return ''
    return new Dayjs(value).format('YYYY-MM-DD HH:mm:ss')
}

Vue.filter('dateFormat', dateFormat)