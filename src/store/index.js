import Vue from 'vue'
import Vuex from 'vuex'
import * as _ from 'lodash'
Vue.use(Vuex)
const fetchData = async(url) => {
  return fetch(url).then( result=>{
    return result.json()
  }).then(data =>{ 
    console.log(data)
    if (data.errCode) {
      const err = new Error(data.message);
      err.errCode = data.errCode;
      throw err
    }
    return data
  }).catch(err =>{
    console.log(`fetch ${url} failed`, err)
    throw err
  })
};
const fetchWeatherData = async({query=""}) => {
  const orginOpenDataApi = "https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=1f1aaba5-616a-4a33-867d-878142cac5c4"
  const backUpApi = "https://fast-meadow-41411.herokuapp.com/taipei_weather_api/weather?t=1"
  const urlList = [orginOpenDataApi, backUpApi]
 
    let result, fetchUrl;
    const tryCount = urlList.length
    for (let idx =0; idx < tryCount; idx++ ) {
      try {
        let apiUrl = urlList[idx]
        fetchUrl = (query && query!=="")?  `${apiUrl}${query}`:`${apiUrl}`
        result = await fetchData(fetchUrl)
        if (result.result.count == 0 && idx == 0) {
          throw Error("empty result");
        } else if (result.result.count > 0) {
          break;
        } 
        
      } catch (error) {
        console.log(`${fetchUrl} fetch data error using backup api`, error);
        result = {result:{limit:1000, offset:0,  sort:"", results:Array(0)}, error: error}
      }
    }
    return result;
}
const filtOnlyLatest = (state) => {
  const originList = state.list
  const groupedData =  _.groupBy(originList, data => data.locationName)
  const finalData = _.mapValues(groupedData, (data)=>{
    return data[data.length-1]
  })
  const flatData = _.flatMap(finalData,(data)=>{
    return {...data}
  })
  state.filteredList = flatData
  state.showList = flatData
}
export default new Vuex.Store({
  state: {
    list: [],
    showList: [],
    filteredList: [],
    isLoading: false,
    showLatest: false
  },
  mutations: {
    async FETCH_WITH_QUERY(state,{query}) {
      try {
        const fetchResult = await fetchWeatherData({query})
        let {results} = fetchResult.result
        results = results.map(data=>{
          let {dataTime} = data
          delete data.dataTime
          return {
            ...data,
            dataTime: new Date(dataTime).getTime(),
          }
        })
        state.list = results
        if (state.showLatest === true){
          filtOnlyLatest(state)
        }
        else {
          state.showList = results
        }
      } catch(err){
        console.log('fetchWeatherData', err)
      }
    },
    async FETCH_WEATHER_DATA(state){
      try {
        const fetchResult = await fetchWeatherData({query:""})
        console.log(fetchResult)
        let {results} = fetchResult.result
        results = results.map(data=>{
          let {dataTime} = data
          delete data.dataTime
          return {
            ...data,
            dataTime: new Date(dataTime).getTime(),
          }
        })
        state.list = results
        state.showList = results
      } catch(err){
        console.log('fetchWeatherData', err)
      }
    },
    FILT_ONLY_LATEST(state) {
      const originList = state.list
      const groupedData =  _.groupBy(originList, data => data.locationName)
      const finalData = _.mapValues(groupedData, (data)=>{
        return data[data.length-1]
      })
      const flatData = _.flatMap(finalData,(data)=>{
        return {...data}
      })
      state.filteredList = flatData
      state.showList = flatData
    },
    SET_LOADING_STATE(state, payload){
      state.isLoading = payload
    },
    UPDATE_SHOW_LATEST(state, payload) {
      state.showLatest = payload
    },
    SHOW_UNFILTERED_LIST(state) {
      state.showList = state.list.slice()
    }
  },
  actions: {
    fetch_weather_data(context) {
      context.commit("FETCH_WEATHER_DATA")
    },
    filt_only_latest(context) {
      context.commit("FILT_ONLY_LATEST")
    },
    set_loading_state(context, payload) {
      context.commit("SET_LOADING_STATE", payload)
    },
    update_show_latest(context, payload) {
      context.commit("UPDATE_SHOW_LATEST", payload)
    },
    show_unfiltered_list(context) {
      context.commit("SHOW_UNFILTERED_LIST")
    },
    fetch_with_query(context, payload) {
      context.commit('FETCH_WITH_QUERY', payload)
    }
  },
  modules: {
  }
})
