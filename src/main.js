import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import '@/filters/index'
import '@fortawesome/free-brands-svg-icons'
import { library } from '@fortawesome/fontawesome-svg-core'
// internal icons
import {
    faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faCalendar, faCalendarDay, faCalendarAlt, faCalendarCheck, faSearch } from "@fortawesome/free-solid-svg-icons"
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome"
library.add(
  faArrowUp, faAngleRight, faAngleLeft, faAngleDown, faCalendarDay, faCalendar, faCalendarAlt, faCalendarCheck, faSearch)
Vue.component('vue-fontawesome', FontAwesomeIcon)
Vue.config.productionTip = false
Vue.use(Buefy, {
  defaultIconComponent: 'vue-fontawesome',
  defaultIconPack: 'fas',
})


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
