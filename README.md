# weather-web-vue

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### My Demo page

[weather-web-vue](https://unkempt-fall.surge.sh/)

### My Design Ideas

1 把 存取api的部份 放在 ACTION 去做
   比如: search
2 資料狀態管理 集中在vuex
    比如: showList 顯示資料
    還有 show only latest 是否只顯示最新資料
3 對資料做整理 使用lodash, 中的 _.groupby, _.flatmap
   來找出群組資料 並且 顯示最新一筆

4 邏輯部份大致上分為兩隻程式
  在store.js的兩隻function

  ```javascript===
  const fetchWeatherData = async({query=""}) => {
  const apiUrl = "https://data.taipei/opendata/datalist/apiAccess?scope=resourceAquire&rid=1f1aaba5-616a-4a33-867d-878142cac5c4"
  const fetchUrl = (query && query!=="")?  `${apiUrl}${query}`:`${apiUrl}`
    return fetch(fetchUrl).then( result=>{
      return result.json()
    }).then(data =>{ 
      return data
    }).catch(err =>{
      console.log(`fetch ${apiUrl} failed`, err)
      return {data:[], error:err}
    })
}

const filtOnlyLatest = (state) => {
  const originList = state.list
  const groupedData =  _.groupBy(originList, data => data.locationName)
  const finalData = _.mapValues(groupedData, (data)=>{
    return data[data.length-1]
  })
  const flatData = _.flatMap(finalData,(data)=>{
    return {...data}
  })
  state.filteredList = flatData
  state.showList = flatData
}
  ```

5 沒做好的部份 由於自己時間沒掌握好
所以沒做unit test在 vuex
還有fetch的部份
